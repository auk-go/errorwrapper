package errbyte

type ResultWithApplicable struct {
	Result
	IsApplicable bool
}
