package linuxservicecmd

import (
	"gitlab.com/auk-go/enum/servicestate"
	"gitlab.com/auk-go/errorwrapper"
)

func SimpleVerifyStatus(serviceName string) *errorwrapper.Wrapper {
	res := Run(
		servicestate.Status,
		serviceName)

	return res.SimplifiedError()
}
