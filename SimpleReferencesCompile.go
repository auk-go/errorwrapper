package errorwrapper

import (
	"fmt"

	"gitlab.com/auk-go/core/constants"
	"gitlab.com/auk-go/core/corecsv"
	"gitlab.com/auk-go/errorwrapper/errconsts"
	"gitlab.com/auk-go/errorwrapper/errtype"
)

func SimpleReferencesCompile(
	errType errtype.Variation,
	references ...interface{},
) string {
	variantStruct := errType.VariantStructure()
	compiledString := corecsv.AnyItemsToCsvString(
		constants.CommaSpace,
		true,
		false,
		references...)

	if compiledString == constants.EmptyString {
		return fmt.Sprintf(
			errconsts.ValueHyphenValueFormat,
			variantStruct.String(),
			variantStruct.Name)
	}

	return fmt.Sprintf(
		errconsts.SimpleReferenceCompileFormat,
		variantStruct.String(),
		variantStruct.Name,
		compiledString)
}
