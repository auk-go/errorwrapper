package errfunc

import (
	"gitlab.com/auk-go/errorwrapper/errwrappers"
)

func ConvertWrapperFuncToIsSuccessCollectorFunc(
	wrapperFunc WrapperFunc,
) IsSuccessCollectorFunc {
	if wrapperFunc == nil {
		return nil
	}

	isSuccessCollectorFunc := func(errorCollection *errwrappers.Collection) (isSuccess bool) {
		errWrapper := wrapperFunc()

		errorCollection.AddWrapperPtr(errWrapper)

		return errWrapper.IsEmptyError()
	}

	return isSuccessCollectorFunc
}
