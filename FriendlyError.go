package errorwrapper

import (
	"strings"

	"gitlab.com/auk-go/core/codestack"
	"gitlab.com/auk-go/core/coredata/coredynamic"
	"gitlab.com/auk-go/core/coredata/corejson"
	"gitlab.com/auk-go/core/coredata/corepayload"
	"gitlab.com/auk-go/core/coreinterface/errcoreinf"
	"gitlab.com/auk-go/core/coretaskinfo"
	"gitlab.com/auk-go/core/corevalidator"
	"gitlab.com/auk-go/core/errcore"
	"gitlab.com/auk-go/enum/logtype"
	"gitlab.com/auk-go/errorwrapper/errtype"
	"gitlab.com/auk-go/errorwrapper/ref"
	"gitlab.com/auk-go/errorwrapper/refs"
)

type FriendlyError struct {
	FriendlyErrorMessage string
	LogType              logtype.Variant
	Info                 *coretaskinfo.Info
	Payloads             []byte
	ErrorWrapper         *Wrapper
}

func (it *FriendlyError) InfoFieldsMap() map[string]string {
	if it == nil {
		return map[string]string{}
	}

	return it.Info.Map()
}

func (it *FriendlyError) SetLogType(variant logtype.Variant) ContractsFriendlyErrorWrapper {
	if it == nil {
		return it
	}

	it.LogType = variant

	return it
}

func (it *FriendlyError) SetTaskInfo(info *coretaskinfo.Info) ContractsFriendlyErrorWrapper {
	if it == nil {
		return it
	}

	it.Info = info

	return it
}

func (it *FriendlyError) TaskInfo() *coretaskinfo.Info {
	if it == nil {
		return nil
	}

	return it.Info
}

func (it *FriendlyError) RawPayloads() []byte {
	if it == nil {
		return nil
	}

	return it.Payloads
}

func (it *FriendlyError) IsEmptyFriendlyMessage() bool {
	return it == nil || it.FriendlyErrorMessage == ""
}

func (it FriendlyError) IsFriendlyMessageContains(
	contains string,
) bool {
	isEmpty := it.IsEmptyFriendlyMessage()

	if isEmpty && contains == "" {
		return true
	} else if isEmpty {
		return false
	}

	return strings.Contains(
		it.FriendlyErrorMessage, contains)
}

func (it *FriendlyError) HasPayload() bool {
	return it != nil && len(it.Payloads) > 0
}

func (it *FriendlyError) IsPayloadEmpty() bool {
	return it == nil || len(it.Payloads) == 0
}

func (it FriendlyError) PayloadDeserialize(
	toPtr interface{},
) *Wrapper {
	if it.IsNull() {
		return NewMsgDisplayErrorNoReference(
			codestack.Skip1,
			errtype.Null,
			"cannot deserialize nil friendly error payload to something.")
	}

	if it.HasError() {
		return it.ErrWrap()
	}

	err := corejson.
		Deserialize.
		UsingBytes(
			it.Payloads,
			toPtr)

	if err == nil {
		return nil
	}

	if it.Info.IsSecure() {
		return NewRefWithMessage(
			codestack.Skip1,
			errtype.Unmarshalling,
			"cannot deserialize, payload data is secured, cannot be logged. check error traces",
			ref.Value{
				Variable: "Friendly Message",
				Value:    it.FriendlyErrorMessage,
			})
	}

	return NewMsgDisplayErrorNoReference(
		codestack.Skip1,
		errtype.Unmarshalling,
		err.Error())
}

func (it FriendlyError) PayloadDeserializeToPayloadWrapper() (
	payloadWrapper *corepayload.PayloadWrapper, compiledErr *Wrapper,
) {
	emptyPw := &corepayload.PayloadWrapper{}
	errWrap := it.PayloadDeserialize(emptyPw)
	if errWrap.HasError() {
		return emptyPw, errWrap
	}

	if emptyPw.HasError() {
		return emptyPw, CastBasicErrWrapperToWrapper(emptyPw.BasicError())
	}

	return emptyPw, nil
}

func (it FriendlyError) JsonModelAny() interface{} {
	return map[string]interface{}{
		"FriendlyErrorMessage": it.FriendlyErrorMessage,
		"LogType":              it.LogType,
		"Info":                 it.Info,
		"IsSecure":             it.Info.IsSecure(),
		"Payloads":             it.PayloadString(),
		"ErrorWrapper":         it.ErrorWrapper,
	}
}

func (it *FriendlyError) MarshalJSON() ([]byte, error) {
	if it == nil {
		return nil, errcore.
			CannotBeNilOrEmptyType.
			ErrorNoRefs("nil, friendly error cannot be marshaled.")
	}

	return corejson.Serialize.Raw(it.JsonModelAny())
}

func (it *FriendlyError) UnmarshalJSON(
	jsonBytes []byte,
) error {
	if it == nil {
		return errcore.
			CannotBeNilOrEmptyType.
			ErrorNoRefs("nil, friendly error cannot be marshaled.")
	}

	var jsonModel map[string]interface{}
	err := corejson.Deserialize.UsingBytes(
		jsonBytes,
		&jsonModel)

	if err == nil {
		// todo refactor, danger code
		it.FriendlyErrorMessage = jsonModel["FriendlyErrorMessage"].(string)
		it.LogType = jsonModel["LogType"].(logtype.Variant)
		it.Info = jsonModel["Info"].(*coretaskinfo.Info)
		it.Payloads = it.SetPayloadString(jsonModel["Payloads"].(string))
		it.ErrorWrapper = jsonModel["ErrorWrapper"].(*Wrapper)
	}

	return err
}

func (it *FriendlyError) MustBeSafe() {
	it.HandleError()
}

func (it *FriendlyError) PayloadString() string {
	if it == nil || len(it.Payloads) == 0 {
		return ""
	}

	return string(it.Payloads)
}

func (it *FriendlyError) SetPayloadString(
	payloadStr string,
) []byte {
	if it == nil {
		return nil
	}

	it.Payloads = []byte(payloadStr)

	return it.Payloads
}

func (it *FriendlyError) SetPayload(
	payloads []byte,
) {
	it.Payloads = payloads
}

func (it *FriendlyError) StackTracesJsonResult() *corejson.Result {
	if it.IsEmpty() {
		return nil
	}

	return it.ErrorWrapper.StackTracesJsonResult()
}

func (it *FriendlyError) StackTraces() string {
	if it.IsEmpty() {
		return ""
	}

	return it.ErrorWrapper.StackTraces()
}

func (it *FriendlyError) NewStackTraces(
	stackSkip int,
) string {
	return codestack.
		NewStacksDefaultCount(stackSkip + defaultSkipInternal).
		CodeStacksString()
}

func (it *FriendlyError) NewDefaultStackTraces() string {
	return codestack.
		NewStacksDefaultCount(defaultSkipInternal).
		CodeStacksString()
}

func (it *FriendlyError) NewStackTracesJsonResult(
	stackSkip int,
) *corejson.Result {
	return codestack.
		NewStacksDefaultCount(defaultSkipInternal + stackSkip).
		JsonPtr()
}

func (it *FriendlyError) NewDefaultStackTracesJsonResult() *corejson.Result {
	return codestack.
		NewStacksDefaultCount(defaultSkipInternal).
		JsonPtr()
}

func (it *FriendlyError) IsBasicErrEqual(
	another errcoreinf.BasicErrWrapper,
) bool {
	if it.IsEmpty() && another == nil || another.IsEmpty() {
		return true
	}

	anotherCasted := CastBasicErrWrapperToWrapper(another)

	return it.IsEquals(anotherCasted)
}

func (it *FriendlyError) MergeNewErrInf(
	right errcoreinf.BaseErrorOrCollectionWrapper,
) errcoreinf.BaseErrorOrCollectionWrapper {
	if right == nil || right.IsEmpty() {
		return it
	}

	return it.ConcatNew().ErrorInterface(right)
}

func (it *FriendlyError) MergeNewMessage(
	newMessage string,
) errcoreinf.BaseErrorOrCollectionWrapper {
	return it.ConcatNew().Message(newMessage)
}

func (it *FriendlyError) CloneInterface() errcoreinf.BasicErrWrapper {
	return it.ClonePtr()
}

func (it *FriendlyError) GetAsBasicWrapperUsingTyper(
	errorTyper errcoreinf.BasicErrorTyper,
) errcoreinf.BasicErrWrapper {
	if it.IsEmpty() {
		return nil
	}

	return NewMsgDisplayError(
		defaultSkipInternal,
		errtype.NewUsingTyper(errorTyper),
		it.ErrorString(),
		it.ErrWrap().References())
}

func (it *FriendlyError) GetAsBasicWrapper() errcoreinf.BasicErrWrapper {
	return it
}

func (it *FriendlyError) ErrWrap() *Wrapper {
	if it == nil || it.ErrorWrapper.IsEmpty() {
		return nil
	}

	return it.ErrorWrapper
}

func (it *FriendlyError) ReferencesList() []errcoreinf.Referencer {
	if it.IsReferencesEmpty() {
		return nil
	}

	return it.ErrWrap().ReferencesList()
}

// ReflectSetToErrWrap
//
//	Reusing ReflectSetTo
func (it *FriendlyError) ReflectSetToErrWrap(toPtr interface{}) *Wrapper {
	err := it.ReflectSetTo(toPtr)

	if err != nil {
		return NewMsgDisplayErrorNoReference(
			defaultSkipInternal,
			errtype.CastingFailed,
			"ReflectSet to cast failed!")
	}

	return nil
}

// IsCollect
//
//	ConcatNew() recommend to use instead.
//
// Warning :
//
//	mutates current error, recommended NOT use,
//	it is for the commonalities between error Wrapper and collection
func (it *FriendlyError) IsCollect(
	another errcoreinf.BaseErrorOrCollectionWrapper,
) bool {
	if another == nil || another.IsEmpty() {
		return false
	}

	return it.ErrWrap().IsCollect(another)
}

// IsCollectedAny
//
//	ConcatNew() recommend to use instead.
//
// Warning :
//
//	mutates current error, recommended NOT use,
//	it is for the commonalities between error Wrapper and collection
func (it *FriendlyError) IsCollectedAny(
	anotherItems ...errcoreinf.BaseErrorOrCollectionWrapper,
) bool {
	if len(anotherItems) == 0 {
		return false
	}

	return it.ErrWrap().
		IsCollectedAny(anotherItems...)
}

// IsCollectOn
//
//	ConcatNew() recommend to use instead.
//
// Warning :
//
//	mutates current error, recommended NOT use,
//	it is for the commonalities between error Wrapper and collection
func (it *FriendlyError) IsCollectOn(
	isCollect bool,
	another errcoreinf.BaseErrorOrCollectionWrapper,
) bool {
	if !isCollect || another == nil || another.IsEmpty() {
		return false
	}

	return it.IsCollect(another)
}

func (it *FriendlyError) IsEmptyAll(
	anotherItems ...errcoreinf.BaseErrorOrCollectionWrapper,
) bool {
	if len(anotherItems) == 0 {
		return true
	}

	return !it.IsCollectedAny(anotherItems...)
}

func (it *FriendlyError) IsDefined() bool {
	return it.HasAnyError()
}

func (it *FriendlyError) HasAnyIssues() bool {
	return it.HasAnyError()
}

func (it *FriendlyError) CompiledJsonErrorWithStackTraces() error {
	if it == nil || it.IsEmpty() {
		return nil
	}

	return it.ErrWrap().
		CompiledJsonErrorWithStackTraces()
}

func (it *FriendlyError) CompiledJsonStringWithStackTraces() (jsonString string) {
	if it == nil || it.IsEmpty() {
		return ""
	}

	return it.ErrWrap().
		CompiledJsonStringWithStackTraces()
}

func (it *FriendlyError) MustBeEmptyError() {
	it.HandleError()
}

func (it *FriendlyError) IsCollectionType() bool {
	return false
}

// ReflectSetTo
//
// # Set any object from to toPointer object
//
// Valid Inputs or Supported (https://t.ly/1Lpt):
//   - From, To: (null, null)                          -- do nothing
//   - From, To: (sameTypePointer, sameTypePointer)    -- try reflection
//   - From, To: (sameTypeNonPointer, sameTypePointer) -- try reflection
//   - From, To: ([]byte or *[]byte, otherType)        -- try unmarshal, reflect
//   - From, To: (otherType, *[]byte)                  -- try marshal, reflect
//
// Validations:
//   - Check null, if both null no error return quickly.
//   - NotSupported returns as error.
//   - NotSupported: (from, to) - (..., not pointer)
//   - NotSupported: (from, to) - (null, notNull)
//   - NotSupported: (from, to) - (notNull, null)
//   - NotSupported: (from, to) - not same type and not bytes on any
//   - `From` null or nil is not supported and will return error.
//
// Reference:
//   - Reflection String Set Example : https://go.dev/play/p/fySLYuOvoRK.go?download=true
//   - Method document screenshot    : https://prnt.sc/26dmf5g
func (it *FriendlyError) ReflectSetTo(toPtr interface{}) error {
	return coredynamic.ReflectSetFromTo(it, toPtr)
}

func (it FriendlyError) ReferencesCollection() errcoreinf.ReferenceCollectionDefiner {
	return it.References()
}

func (it *FriendlyError) IsNoError() bool {
	return it == nil || it.ErrWrap().Type() == errtype.NoError
}

func (it *FriendlyError) MustBeEmpty() {
	it.HandleError()
}

func (it *FriendlyError) IsInvalid() bool {
	return it.HasError()
}

func (it *FriendlyError) HasAnyError() bool {
	return it.HasError()
}

// Compile
//
//	Refers to the FullString
func (it *FriendlyError) Compile() string {
	return it.FullString()
}

// CompileString
//
//	Refers to the FullString
func (it *FriendlyError) CompileString() string {
	return it.FullString()
}

func (it *FriendlyError) FullStringWithTracesIf(
	isStackTraces bool,
) string {
	if it.IsEmpty() {
		return ""
	}

	if isStackTraces {
		return it.FullStringWithTraces()
	}

	return it.FullString()
}

func (it *FriendlyError) ReferencesCompiledString() string {
	if it.IsEmpty() || it.IsReferencesEmpty() {
		return ""
	}

	return it.References().String()
}

func (it *FriendlyError) CompiledStackTracesString() string {
	if it.IsEmpty() {
		return ""
	}

	return it.StackTraceString()
}

func (it *FriendlyError) SerializeWithoutTraces() ([]byte, error) {
	if it.IsEmpty() {
		return nil, nil
	}

	cloned := it.cloneWithoutStacktrace()

	return cloned.JsonPtr().Raw()
}

// Serialize
//
//	Returns json with stack-traces
func (it *FriendlyError) Serialize() ([]byte, error) {
	if it.IsEmpty() {
		return nil, nil
	}

	return it.JsonPtr().Raw()
}

func (it *FriendlyError) SerializeMust() []byte {
	if it.IsEmpty() {
		return nil
	}

	rawJsonBytes, err := it.JsonPtr().Raw()
	errcore.MustBeEmpty(err)

	return rawJsonBytes
}

func (it *FriendlyError) LogIf(isLog bool) {
	if isLog {
		it.Log()
	}
}

func (it *FriendlyError) HasReferences() bool {
	return it != nil &&
		it.ErrWrap().HasReferences()
}

func (it *FriendlyError) TypeNameCodeMessage() string {
	if it.IsEmpty() {
		return ""
	}

	return it.ErrWrap().TypeNameCodeMessage()
}

func (it *FriendlyError) RawErrorTypeValue() uint16 {
	if it.IsEmpty() {
		return errtype.NoError.ValueUInt16()
	}

	return it.ErrWrap().RawErrorTypeValue()
}

func (it *FriendlyError) CodeTypeName() string {
	return it.TypeName()
}

func (it *FriendlyError) ErrorTypeAsBasicErrorTyper() errcoreinf.BasicErrorTyper {
	return it.ErrWrap().ErrorTypeAsBasicErrorTyper()
}

func (it *FriendlyError) StackTraceString() string {
	return it.ErrWrap().StackTraceString()
}

func (it *FriendlyError) References() *refs.Collection {
	return it.ErrWrap().References()
}

func (it *FriendlyError) CloneReferences() *refs.Collection {
	return it.ErrWrap().CloneReferences()
}

func (it *FriendlyError) MergeNewReferences(
	additionalReferences ...ref.Value,
) *refs.Collection {
	return it.ErrWrap().
		MergeNewReferences(additionalReferences...)
}

// HasError
//
// Returns true if not empty. Invert of IsEmpty()
//
// Conditions (true):
//   - if Wrapper is NOT nil, Or,
//   - if Wrapper is NOT StaticEmptyPtr, Or,
//   - if Wrapper .errorType is NOT IsNoError(), Or,
//   - if Wrapper .currentError is nil and Wrapper .references.IsEmpty()
func (it *FriendlyError) HasError() bool {
	return !it.IsEmpty()
}

// HasCurrentError
//
// Refers to Wrapper error embedded or not.
//
// Best to use HasError
func (it *FriendlyError) HasCurrentError() bool {
	return it.ErrWrap().HasCurrentError()
}

func (it *FriendlyError) Type() errtype.Variation {
	return it.ErrWrap().Type()
}

// errorTypeOrGeneric
//
//	return errtype.Generic if empty or noError type,
//	or else returns the exact type
func (it *FriendlyError) errorTypeOrGeneric() errtype.Variation {
	return it.ErrWrap().errorTypeOrGeneric()
}

func (it *FriendlyError) GetTypeVariantStruct() errtype.VariantStructure {
	return it.ErrWrap().GetTypeVariantStruct()
}

// TypeString
//
//	Returns whole type string, should be refactored to whole-type string name
//
//	Format :
//	 - errconsts.VariantStructStringFormat
//	 - "%s (Code - %d) : %s" : "Name (Code - ValueInt) : Message from type string"
//	 - Exact Example for errtype.Generic : "Generic (Code - 1) : Generic error"
func (it *FriendlyError) TypeString() string {
	return it.ErrWrap().TypeString()
}

// TypeNameCode
//
//	Returns error type Code number value with name,
//	on empty returns empty string.
//
// Format :
//   - "(#%d - %s)"
//   - "(#1 - Generic)"
func (it *FriendlyError) TypeNameCode() string {
	return it.ErrWrap().TypeNameCode()
}

// TypeName
//
//	Returns error type name,
//	on empty returns empty string.
//
// Example :
//   - For errtype.NoError : ""
//   - For errtype.Generic : "Generic"
func (it *FriendlyError) TypeName() string {
	return it.ErrWrap().TypeName()
}

// TypeNameWithCustomMessage
//
//	errconsts.ErrorCodeHyphenTypeNameWithLineFormat = "(#%d - %s) %s"
func (it *FriendlyError) TypeNameWithCustomMessage(
	customMessage string,
) string {
	return it.ErrWrap().
		TypeNameWithCustomMessage(customMessage)
}

// TypeCodeNameString
//
// Format : errconsts.ErrorCodeWithTypeNameFormat
//
//	"(Code - #%d) : %s"
func (it *FriendlyError) TypeCodeNameString() string {
	return it.ErrWrap().
		TypeCodeNameString()
}

func (it *FriendlyError) IsTypeOf(errType errtype.Variation) bool {
	return it.ErrWrap().
		IsTypeOf(errType)
}

func (it *FriendlyError) IsEmptyError() bool {
	return it.ErrWrap().
		IsEmptyError()
}

func (it *FriendlyError) Error() error {
	if it.IsEmpty() {
		return nil
	}

	return it.ErrWrap().
		Error()
}

// ErrorString if empty error then returns ""
func (it *FriendlyError) ErrorString() string {
	return it.ErrWrap().
		ErrorString()
}

func (it *FriendlyError) IsReferencesEmpty() bool {
	return it.ErrWrap().
		IsReferencesEmpty()
}

func (it *FriendlyError) IsNull() bool {
	return it == nil
}

func (it *FriendlyError) IsAnyNull() bool {
	return it == nil || it.ErrorWrapper.IsAnyNull()
}

// IsEmpty
//
// Refers to no error for print or doesn't treat this as error.
//
// Conditions (true):
//   - if Wrapper nil, Or,
//   - if Wrapper is StaticEmptyPtr, Or,
//   - if Wrapper .errorType is IsNoError(), Or,
//   - if Wrapper .currentError NOT nil and Wrapper .references.IsEmpty()
func (it *FriendlyError) IsEmpty() bool {
	if it == nil {
		return true
	}

	return it.ErrWrap().IsEmpty()
}

func (it *FriendlyError) IsSuccess() bool {
	return it.IsEmpty()
}

func (it *FriendlyError) IsValid() bool {
	return it.IsEmpty()
}

func (it *FriendlyError) IsFailed() bool {
	return it.HasError()
}

func (it *FriendlyError) IsNotEquals(right *Wrapper) bool {
	return !it.IsEquals(right)
}

func (it *FriendlyError) IsEquals(right *Wrapper) bool {
	return it.ErrWrap().IsEquals(right)
}

func (it *FriendlyError) IsErrorEquals(err error) bool {
	return it.ErrWrap().IsErrorEquals(err)
}

func (it *FriendlyError) IsErrorMessageEqual(msg string) bool {
	return it.IsErrorMessage(msg, true)
}

// IsErrorMessage
//
// If error IsEmpty then returns false regardless
func (it *FriendlyError) IsErrorMessage(msg string, isCaseSensitive bool) bool {
	return it.ErrWrap().
		IsErrorMessage(msg, isCaseSensitive)
}

// IsErrorMessageContains If error IsEmpty then returns false regardless
func (it *FriendlyError) IsErrorMessageContains(
	msg string,
	isCaseSensitive bool,
) bool {
	return it.ErrWrap().
		IsErrorMessage(msg, isCaseSensitive)
}

// HandleError
//
// Only call panic if Wrapper has currentError
func (it *FriendlyError) HandleError() {
	if it.IsEmpty() {
		return
	}

	it.ErrWrap().HandleError()
}

// HandleErrorWithMsg Only call panic if has currentError
func (it *FriendlyError) HandleErrorWithMsg(
	newMessage string,
) {
	if it.IsEmpty() {
		return
	}

	it.ErrWrap().HandleErrorWithMsg(newMessage)
}

func (it *FriendlyError) HandleErrorWithRefs(
	newMessage string,
	refVar,
	refVal interface{},
) {
	it.ErrWrap().HandleErrorWithRefs(
		newMessage,
		refVar,
		refVal)
}

func (it *FriendlyError) Value() error {
	return it.ErrWrap().Value()
}

func (it *FriendlyError) FullStringSplitByNewLine() []string {
	return it.ErrWrap().FullStringSplitByNewLine()
}

func (it *FriendlyError) FullString() string {
	return it.ErrWrap().FullString()
}

func (it *FriendlyError) FullStringWithoutReferences() string {
	return it.ErrWrap().FullStringWithoutReferences()
}

func (it *FriendlyError) FullStringWithTraces() string {
	return it.ErrWrap().FullStringWithTraces()
}

func (it *FriendlyError) StackTracesLimit(limit int) *codestack.TraceCollection {
	return it.ErrWrap().StackTracesLimit(limit)
}

func (it *FriendlyError) FullStringWithLimitTraces(limit int) string {
	return it.ErrWrap().FullStringWithLimitTraces(limit)
}

func (it *FriendlyError) getRefsCollectionCompiled() string {
	return it.ErrWrap().getRefsCollectionCompiled()
}

func (it *FriendlyError) finalMessageWithCategory() string {
	refsCompiled := it.getRefsCollectionCompiled()

	return it.finalMessageWithoutRefPlusCategory(refsCompiled)
}

func (it *FriendlyError) finalMessageWithoutRefPlusCategory(
	referencesCompiled string,
) string {
	return it.ErrWrap().
		finalMessageWithoutRefPlusCategory(referencesCompiled)
}

func (it *FriendlyError) RawErrorTypeName() string {
	return it.ErrWrap().
		RawErrorTypeName()
}

func (it FriendlyError) String() string {
	return it.ErrWrap().
		String()
}

func (it FriendlyError) StringIf(isWithRef bool) string {
	return it.ErrWrap().
		StringIf(isWithRef)
}

func (it *FriendlyError) CompiledError() error {
	return it.ErrWrap().
		CompiledError()
}

func (it *FriendlyError) CompiledErrorWithStackTraces() error {
	return it.ErrWrap().
		CompiledErrorWithStackTraces()
}

func (it *FriendlyError) FullOrErrorMessage(
	isErrorMessage,
	isWithRef bool,
) string {
	return it.ErrWrap().
		FullOrErrorMessage(
			isErrorMessage,
			isWithRef)
}

func (it *FriendlyError) Log() {
	it.ErrWrap().
		Log()
}

func (it *FriendlyError) LogWithTraces() {
	it.ErrWrap().
		LogWithTraces()
}

func (it *FriendlyError) LogFatal() {
	it.ErrWrap().
		LogFatal()
}

func (it *FriendlyError) LogFatalWithTraces() {
	it.ErrWrap().
		LogFatalWithTraces()
}

// ConcatNew
//
//	It is safe to use for nil.
func (it *FriendlyError) ConcatNew() ConcatNew {
	return ConcatNew{
		errWrap: it.ErrWrap(),
	}
}

func (it *FriendlyError) Dispose() {
	if it == nil {
		return
	}

	it.Payloads = nil
	it.Info = nil
	it.ErrorWrapper.Dispose()
	it.ErrorWrapper = nil
}

func (it FriendlyError) JsonModel() WrapperDataModel {
	return it.ErrWrap().JsonModel()
}

func (it FriendlyError) JsonResultWithoutTraces() *corejson.Result {
	if it.IsEmpty() {
		return nil
	}

	return corejson.NewPtr(
		it.cloneWithoutStacktrace())
}

func (it FriendlyError) Json() corejson.Result {
	return corejson.New(it)
}

func (it FriendlyError) JsonPtr() *corejson.Result {
	return corejson.NewPtr(it)
}

func (it *FriendlyError) ParseInjectUsingJson(
	jsonResult *corejson.Result,
) (*FriendlyError, error) {
	err := jsonResult.Unmarshal(it)

	if err != nil {
		return it, err
	}

	return it, nil
}

// ParseInjectUsingJsonMust Panic if error
//
//goland:noinspection GoLinterLocal
func (it *FriendlyError) ParseInjectUsingJsonMust(
	jsonResult *corejson.Result,
) *FriendlyError {
	newUsingJson, err :=
		it.ParseInjectUsingJson(jsonResult)

	if err != nil {
		panic(err)
	}

	return newUsingJson
}

func (it *FriendlyError) JsonParseSelfInject(
	jsonResult *corejson.Result,
) error {
	_, err := it.ParseInjectUsingJson(
		jsonResult,
	)

	return err
}

func (it *FriendlyError) ValidationErrUsingTextValidator(
	validator *corevalidator.TextValidator,
	params *corevalidator.ValidatorParamsBase,
) *Wrapper {
	err := validator.VerifyDetailError(
		params,
		it.FullString())

	if err != nil {
		return NewUsingError(
			defaultSkipInternal,
			errtype.ValidationMismatch,
			err)
	}

	return StaticEmptyPtr
}

func (it FriendlyError) AsJsonContractsBinder() corejson.JsonContractsBinder {
	return &it
}

func (it FriendlyError) AsErrorWrapper() *Wrapper {
	return it.ErrWrap()
}

func (it FriendlyError) NonPtr() FriendlyError {
	return it
}

func (it FriendlyError) Ptr() *FriendlyError {
	return &it
}

func (it *FriendlyError) ClonePtr() *FriendlyError {
	if it == nil {
		return nil
	}

	return &FriendlyError{
		FriendlyErrorMessage: it.FriendlyErrorMessage,
		LogType:              it.LogType,
		Info:                 it.Info.ClonePtr(),
		Payloads:             it.Payloads,
		ErrorWrapper:         it.ErrWrap().ClonePtr(),
	}
}

func (it FriendlyError) Clone() FriendlyError {
	cloned := it.ClonePtr()

	if cloned == nil {
		return FriendlyError{}
	}

	return cloned.NonPtr()
}

func (it *FriendlyError) FriendlyMsg() string {
	if it == nil {
		return ""
	}

	return it.FriendlyErrorMessage
}

func (it FriendlyError) cloneWithoutStacktrace() Wrapper {
	return it.ErrWrap().cloneWithoutStacktrace()
}

func (it FriendlyError) CloneNewStackSkipPtr(stackSkip int) *Wrapper {
	return it.ErrWrap().CloneNewStackSkipPtr(stackSkip)
}

func (it FriendlyError) AsContractsFriendlyErrorWrapper() FriendlyErrorWrapperContractsBinder {
	return &it
}
