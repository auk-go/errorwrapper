package linuxservicecmd

import (
	"gitlab.com/auk-go/core/cmdconsts"
	"gitlab.com/auk-go/enum/servicestate"
	"gitlab.com/auk-go/errorwrapper/errcmd"
)

func getCmdOnce(
	action servicestate.Action,
	serviceName string,
) *errcmd.CmdOnce {
	actionName := action.Name()

	if hasSystemCtlService {
		return bashArgsCmdCreator(
			cmdconsts.SystemCtl,
			actionName,
			serviceName,
		)
	}

	if hasService {
		return bashArgsCmdCreator(
			cmdconsts.Service,
			serviceName,
			actionName)
	}

	return bashArgsCmdCreator(
		cmdconsts.SystemCtl,
		actionName,
		serviceName,
	)
}
