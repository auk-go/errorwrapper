package creationtests

import (
	"errors"

	"gitlab.com/auk-go/core/coredata/corejson"
	"gitlab.com/auk-go/core/coreinterface/errcoreinf"
	"gitlab.com/auk-go/core/coretests"
	"gitlab.com/auk-go/errorwrapper/errdata/errany"
	"gitlab.com/auk-go/errorwrapper/errdata/errbool"
	"gitlab.com/auk-go/errorwrapper/errdata/errbyte"
	"gitlab.com/auk-go/errorwrapper/errdata/errfloat"
	"gitlab.com/auk-go/errorwrapper/errdata/errfloat64"
	"gitlab.com/auk-go/errorwrapper/errdata/errint"
	"gitlab.com/auk-go/errorwrapper/errdata/errjson"
	"gitlab.com/auk-go/errorwrapper/errdata/errstr"
	"gitlab.com/auk-go/errorwrapper/errnew"
	"gitlab.com/auk-go/errorwrapper/errtype"
	"gitlab.com/auk-go/errorwrapper/errwrappers"
)

var testCases = []TestCaseWrapper{
	{
		BaseTestCase: coretests.BaseTestCase{
			Title: "ErrorWrapper creation tests",
			ArrangeInput: []errcoreinf.BaseErrorOrCollectionWrapper{
				errnew.Null.Error(errors.New("something is null")),
				errnew.Null.Simple(nil),
				errnew.Null.Simple(nilErr),
				errnew.Null.WithMessage("error is null", nilErr),
				errnew.Type.Create(errtype.AccountExpired),
				passwordCrudErr,
				errnew.Type.DirectRefs(errtype.IdMissing, "some ids missing", "id1", "id2"),
				errnew.Refs.TypeQuick(errtype.FailedProcess, "process 1", nilErr, 1, "id1", "id2"),
				errwrappers.
					Empty().
					AddWrapperPtr(
						errnew.NotFound.Invalid(
							"something is invalid", "s1", "s2")),
				errwrappers.
					Empty().
					NullSimple(nilErr),
				errany.New.Result.ErrorWrapper(passwordCrudErr).ErrorWrapper,
				errbool.New.Result.ErrorWrapper(passwordCrudErr).ErrorWrapper,
				errbyte.New.Result.ErrorWrapper(passwordCrudErr).ErrorWrapper,
				errfloat.New.Result.ErrorWrapper(passwordCrudErr).ErrorWrapper,
				errfloat64.New.Result.ErrorWrapper(passwordCrudErr).ErrorWrapper,
				errint.New.Result.ErrorWrapper(passwordCrudErr).ErrorWrapper,
				errjson.New.Result.ErrorWrapper(passwordCrudErr).ErrorWrapper,
				errstr.New.Result.ErrorWrapper(passwordCrudErr).ErrorWrapper,
				errstr.New.Result.UsingJsonResult(
					errtype.ExpectingFileButDir,
					errnew.
						Friendly.
						Create("friendly message", passwordCrudErr).
						JsonPtr(),
				).ErrorWrapper,
				errnew.Friendly.
					Create("friendly message", passwordCrudErr),
				errnew.Friendly.
					UrlExamples(
						"friendly message",
						passwordCrudErr,
						"sample task 1",
						"task desc 2",
						"some url example",
						"example 1 : do this",
						"example 2 : do this"),
				errnew.Ref.
					MsgWithJsonResult(
						errtype.Unmarshalling,
						"json result error testing",
						"some payload",
						&corejson.Result{
							Bytes:    passwordCrudErr.SerializeWithoutTracesMust(),
							Error:    errors.New("some error"),
							TypeName: "test type",
						}),

				errnew.Ref.
					MsgWithJsonResult(
						errtype.Unmarshalling,
						"json result error testing",
						"some payload",
						&corejson.Result{
							Bytes:    passwordCrudErr.SerializeWithoutTracesMust(),
							TypeName: "test no err type",
						}),
			},
			ExpectedInput: []string{
				"Case Index : 0, Error Index: 0, Full string: [Error (Null - #3): Null reference. something is null.], Json : {\"IsDisplayableError\":true,\"CurrentError\":\"something is null\",\"ErrorType\":{\"Category\":\"Null\",\"Id\":3},\"StackTraces\":{},\"References\":null,\"HasError\":true}",
				"Case Index : 0, Error Index: 1, Full string: [Error (Null - #3): Null reference. Ref(s) {[Type (string): \"interface{}.(nil)\"]}], Json : {\"IsDisplayableError\":true,\"CurrentError\":\"\",\"ErrorType\":{\"Category\":\"Null\",\"Id\":3},\"StackTraces\":{},\"References\":[{\"VariableName\":\"Type\",\"ValueString\":\"interface{}.(nil)\"}],\"HasError\":true}",
				"Case Index : 0, Error Index: 2, Full string: [Error (Null - #3): Null reference. Ref(s) {[Type (string): \"interface{}.(nil)\"]}], Json : {\"IsDisplayableError\":true,\"CurrentError\":\"\",\"ErrorType\":{\"Category\":\"Null\",\"Id\":3},\"StackTraces\":{},\"References\":[{\"VariableName\":\"Type\",\"ValueString\":\"interface{}.(nil)\"}],\"HasError\":true}",
				"Case Index : 0, Error Index: 3, Full string: [Error (Null - #3): Null reference. Additional : error is null. Ref(s) {[Type (string): \"interface{}.(nil)\"]}], Json : {\"IsDisplayableError\":true,\"CurrentError\":\"error is null\",\"ErrorType\":{\"Category\":\"Null\",\"Id\":3},\"StackTraces\":{},\"References\":[{\"VariableName\":\"Type\",\"ValueString\":\"interface{}.(nil)\"}],\"HasError\":true}",
				"Case Index : 0, Error Index: 4, Full string: [Error (AccountExpired - #896): Account(s) expired!], Json : ",
				"Case Index : 0, Error Index: 5, Full string: [Error (PasswordCrud - #914): Password CRUD failed! some password.], Json : {\"IsDisplayableError\":true,\"CurrentError\":\"some password\",\"ErrorType\":{\"Category\":\"PasswordCrud\",\"Id\":914},\"StackTraces\":{},\"References\":null,\"HasError\":true}",
				"Case Index : 0, Error Index: 6, Full string: [Error (IdMissing - #569): Id not found or missing or undefined! Additional : some ids missing. Ref(s) {[References (string): \"\"id1\", \"id2\"\"]}], Json : {\"IsDisplayableError\":true,\"CurrentError\":\"some ids missing\",\"ErrorType\":{\"Category\":\"IdMissing\",\"Id\":569},\"StackTraces\":{},\"References\":[{\"VariableName\":\"References\",\"ValueString\":\"\\\"id1\\\", \\\"id2\\\"\"}],\"HasError\":true}",
				"Case Index : 0, Error Index: 7, Full string: [Error (FailedProcess - #119): Requested process or task is failed. Additional : \"process 1\", \"<nil>\", \"1\", \"id1\", \"id2\". Ref(s) {[: (string): \"\"process 1\", \"<nil>\", \"1\", \"id1\", \"id2\"\"]}], Json : {\"IsDisplayableError\":true,\"CurrentError\":\"\\\"process 1\\\", \\\"\\u003cnil\\u003e\\\", \\\"1\\\", \\\"id1\\\", \\\"id2\\\"\",\"ErrorType\":{\"Category\":\"FailedProcess\",\"Id\":119},\"StackTraces\":{},\"References\":[{\"VariableName\":\":\",\"ValueString\":\"\\\"process 1\\\", \\\"\\u003cnil\\u003e\\\", \\\"1\\\", \\\"id1\\\", \\\"id2\\\"\"}],\"HasError\":true}",
				"Case Index : 0, Error Index: 8, Full string: # Error Wrappers - Collection - Length[1]\n\n- [Error (Invalid - #271): Invalid! Additional : something is invalid. Ref(s) {[References ([]interface {}): \"[s1 s2]\"]}], Json : []",
				"Case Index : 0, Error Index: 9, Full string: # Error Wrappers - Collection - Length[1]\n\n- [Error (Null - #3): Null reference. Ref(s) {[Type (string): \"interface{}.(nil)\"]}], Json : []",
				"Case Index : 0, Error Index: 10, Full string: [Error (PasswordCrud - #914): Password CRUD failed! some password.], Json : {\"IsDisplayableError\":true,\"CurrentError\":\"some password\",\"ErrorType\":{\"Category\":\"PasswordCrud\",\"Id\":914},\"StackTraces\":{},\"References\":null,\"HasError\":true}",
				"Case Index : 0, Error Index: 11, Full string: [Error (PasswordCrud - #914): Password CRUD failed! some password.], Json : {\"IsDisplayableError\":true,\"CurrentError\":\"some password\",\"ErrorType\":{\"Category\":\"PasswordCrud\",\"Id\":914},\"StackTraces\":{},\"References\":null,\"HasError\":true}",
				"Case Index : 0, Error Index: 12, Full string: [Error (PasswordCrud - #914): Password CRUD failed! some password.], Json : {\"IsDisplayableError\":true,\"CurrentError\":\"some password\",\"ErrorType\":{\"Category\":\"PasswordCrud\",\"Id\":914},\"StackTraces\":{},\"References\":null,\"HasError\":true}",
				"Case Index : 0, Error Index: 13, Full string: [Error (PasswordCrud - #914): Password CRUD failed! some password.], Json : {\"IsDisplayableError\":true,\"CurrentError\":\"some password\",\"ErrorType\":{\"Category\":\"PasswordCrud\",\"Id\":914},\"StackTraces\":{},\"References\":null,\"HasError\":true}",
				"Case Index : 0, Error Index: 14, Full string: [Error (PasswordCrud - #914): Password CRUD failed! some password.], Json : {\"IsDisplayableError\":true,\"CurrentError\":\"some password\",\"ErrorType\":{\"Category\":\"PasswordCrud\",\"Id\":914},\"StackTraces\":{},\"References\":null,\"HasError\":true}",
				"Case Index : 0, Error Index: 15, Full string: [Error (PasswordCrud - #914): Password CRUD failed! some password.], Json : {\"IsDisplayableError\":true,\"CurrentError\":\"some password\",\"ErrorType\":{\"Category\":\"PasswordCrud\",\"Id\":914},\"StackTraces\":{},\"References\":null,\"HasError\":true}",
				"Case Index : 0, Error Index: 16, Full string: [Error (PasswordCrud - #914): Password CRUD failed! some password.], Json : {\"IsDisplayableError\":true,\"CurrentError\":\"some password\",\"ErrorType\":{\"Category\":\"PasswordCrud\",\"Id\":914},\"StackTraces\":{},\"References\":null,\"HasError\":true}",
				"Case Index : 0, Error Index: 17, Full string: [Error (PasswordCrud - #914): Password CRUD failed! some password.], Json : {\"IsDisplayableError\":true,\"CurrentError\":\"some password\",\"ErrorType\":{\"Category\":\"PasswordCrud\",\"Id\":914},\"StackTraces\":{},\"References\":null,\"HasError\":true}",
				"Case Index : 0, Error Index: 18, Full string: , Json : ",
				"Case Index : 0, Error Index: 19, Full string: [Error (PasswordCrud - #914): Password CRUD failed! some password.], Json : {\"IsDisplayableError\":true,\"CurrentError\":\"some password\",\"ErrorType\":{\"Category\":\"PasswordCrud\",\"Id\":914},\"StackTraces\":{},\"References\":null,\"HasError\":true}",
				"Case Index : 0, Error Index: 20, Full string: [Error (PasswordCrud - #914): Password CRUD failed! Additional : some password. Ref(s) {[Description (string): \"task desc 2\"], [Examples (string): \"example 1 : do this, example 2 : do this\"], [Name (string): \"sample task 1\"], [Url (string): \"some url example\"]}], Json : {\"IsDisplayableError\":true,\"CurrentError\":\"some password\",\"ErrorType\":{\"Category\":\"PasswordCrud\",\"Id\":914},\"StackTraces\":{},\"References\":[{\"VariableName\":\"Name\",\"ValueString\":\"sample task 1\"},{\"VariableName\":\"Description\",\"ValueString\":\"task desc 2\"},{\"VariableName\":\"Url\",\"ValueString\":\"some url example\"},{\"VariableName\":\"Examples\",\"ValueString\":\"example 1 : do this, example 2 : do this\"}],\"HasError\":true}",
				"Case Index : 0, Error Index: 21, Full string: [Error (Unmarshalling - #205): Unmarshalling or deserializing failed, cannot process the request further. Additional : json result error testing. Ref(s) {[some payload (string): \"\"], [some payload.error (string): \"Failed : request failed to parse! Bytes data either nil or empty. Additional: some error, type: Ref(s) { \"test type\" }\"]}], Json : {\"IsDisplayableError\":true,\"CurrentError\":\"json result error testing\",\"ErrorType\":{\"Category\":\"Unmarshalling\",\"Id\":205},\"StackTraces\":{},\"References\":[{\"VariableName\":\"some payload.error\",\"ValueString\":\"Failed : request failed to parse! Bytes data either nil or empty. Additional: some error, type: Ref(s) { \\\"test type\\\" }\"},{\"VariableName\":\"some payload\",\"ValueString\":\"\"}],\"HasError\":true}",
				"Case Index : 0, Error Index: 22, Full string: [Error (Unmarshalling - #205): Unmarshalling or deserializing failed, cannot process the request further. Additional : json result error testing. Ref(s) {[some payload (string): \"{\"IsDisplayableError\":true,\"CurrentError\":\"some password\",\"ErrorType\":{\"Category\":\"PasswordCrud\",\"Id\":914},\"StackTraces\":{},\"References\":null,\"HasError\":true}\"]}], Json : {\"IsDisplayableError\":true,\"CurrentError\":\"json result error testing\",\"ErrorType\":{\"Category\":\"Unmarshalling\",\"Id\":205},\"StackTraces\":{},\"References\":[{\"VariableName\":\"some payload\",\"ValueString\":\"{\\\"IsDisplayableError\\\":true,\\\"CurrentError\\\":\\\"some password\\\",\\\"ErrorType\\\":{\\\"Category\\\":\\\"PasswordCrud\\\",\\\"Id\\\":914},\\\"StackTraces\\\":{},\\\"References\\\":null,\\\"HasError\\\":true}\"}],\"HasError\":true}",
			},
		},
	},
}
