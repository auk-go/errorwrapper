package errfunc

import (
	"gitlab.com/auk-go/enum/linuxtype"
)

type LinuxIsSuccessPlusErrorCollectAction struct {
	LinuxType linuxtype.Variant
	IsSuccessCollectorFunc
}
