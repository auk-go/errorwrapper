package linuxservicecmd

import (
	"gitlab.com/auk-go/enum/servicestate"
	"gitlab.com/auk-go/errorwrapper"
)

func RestartQuick(
	servicesName string,
) *errorwrapper.Wrapper {
	_, errWrap := RunAction(
		true,
		false,
		servicestate.Restart,
		servicesName)

	return errWrap
}
