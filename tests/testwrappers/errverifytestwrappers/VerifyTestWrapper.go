package errverifytestwrappers

import (
	"gitlab.com/auk-go/errorwrapper"
	"gitlab.com/auk-go/errorwrapper/errverify"
)

type VerifyTestWrapper struct {
	ErrorWrapper *errorwrapper.Wrapper
	Verifier     errverify.Verifier
}
