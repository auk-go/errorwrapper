package errnew

import (
	"gitlab.com/auk-go/errorwrapper/errtype"
)

var (
	Messages                      = newMessagesToErrorWrapperCreator{}
	Message                       = newMessageToErrorWrapperCreator{}
	Enum                          = newEnumToErrorWrapperCreator{}
	Ref                           = newRefToErrorWrapperCreator{}
	Refs                          = newReferencesToErrorWrapperCreator{}
	Path                          = newPathToErrorWrapperCreator{}
	Merge                         = newMergeToErrorWrapperCreator{}
	FromTo                        = newFromToErrorWrapperCreator{}
	Null                          = newNullToErrorWrapperCreator{}
	Type                          = newTypeToWrapperCreator{}
	Friendly                      = newFriendlyErrorCreator{}
	Error                         = newErrorToWrapperCreator{}
	Range                         = newRangeWrapperCreator{}
	Unmarshal                     = newUnmarshalWrapperCreator{} // refers to unmarshal related quick errors
	Fmt                           = newFormatterCreator{}
	MessageWithRef                = newMessageWithRefCreator{}
	Json                          = newJsonToWrapperCreator{}
	Payload                       = newPayloadToErrorWrapperCreator{}
	Reflect                       = newReflectErrToWrapperCreator{}
	DeserializeTo                 = newDeserializeToWrapperCreator{} // actually helps to deserialize to something
	ErrInterface                  = newErrorInterfaceToWrapperCreator{}
	SrcDst                        = newSourceDestinationToErrorWrapperCreator{}
	MappingFailed                 = Messages.Single(errtype.MappingFailed, "Cannot map the given data to expected data format from the dictionary or map.")
	InvalidSystemUser             = Messages.Single(errtype.SysUserInvalid, "System user not found or id has issues.")
	InvalidSystemGroup            = Messages.Single(errtype.SysGroupInvalid, "System group not found or id has issues.")
	FinalizedResourceCannotAccess = Type.Create(errtype.FinalizedResourceCannotAccess)
	NotFound                      = newNotFoundErrCreator{}
	EmptyString                   = Type.Create(errtype.EmptyString)
	Unexpected                    = Type.Create(errtype.Unexpected)
	InvalidInput                  = Type.Create(errtype.InvalidInput)
	Invalid                       = Type.Create(errtype.Invalid)
	InvalidOption                 = Type.Create(errtype.InvalidOption)
)
