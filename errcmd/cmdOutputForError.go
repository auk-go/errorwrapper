package errcmd

import (
	"os/exec"

	"gitlab.com/auk-go/core/conditional"
	"gitlab.com/auk-go/core/constants"
	"gitlab.com/auk-go/core/coredata/corestr"
	"gitlab.com/auk-go/core/errcore"
	"gitlab.com/auk-go/errorwrapper/errtype"
)

func cmdOutputForError(
	stdIn *StdIn,
	wholeCmdString string,
	allBytes []byte,
	cmd *exec.Cmd,
) *cmdCompiledOutput {
	hasStdOut := stdIn.HasStdOut()
	hasStdErr := stdIn.HasStdErr()

	stdOutString := conditional.StringTrueFunc(
		hasStdOut,
		stdIn.StdOutString,
	)

	stdErrString := conditional.StringTrueFunc(
		hasStdOut,
		stdIn.StdErrString)

	references := constants.EmptyString

	if hasStdErr || hasStdOut {
		references = errcore.VarTwoNoType(
			"OutBuffer", stdOutString,
			"ErrBuffer", stdErrString)
	}

	errFinal := errtype.FailedProcess.ErrorReferences(
		"Command:"+wholeCmdString,
		references)

	return &cmdCompiledOutput{
		Cmd:         cmd,
		Output:      corestr.New.SimpleStringOnce.Init(string(allBytes)),
		Error:       errFinal,
		ProcessName: cmd.Path,
		Arguments:   cmd.Args,
	}
}
