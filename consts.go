package errorwrapper

import "gitlab.com/auk-go/core/constants"

const (
	defaultSkipInternal = constants.One
	MessagesJoiner      = constants.Space
)
