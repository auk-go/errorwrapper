package creationtests

import (
	"errors"

	"gitlab.com/auk-go/errorwrapper/errnew"
	"gitlab.com/auk-go/errorwrapper/errtype"
)

var (
	nilErr          error = nil
	passwordCrudErr       = errnew.
			Type.
			Error(errtype.PasswordCrud, errors.New("some password"))
)
