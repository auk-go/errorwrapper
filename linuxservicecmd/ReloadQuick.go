package linuxservicecmd

import (
	"gitlab.com/auk-go/enum/servicestate"
	"gitlab.com/auk-go/errorwrapper"
)

func ReloadQuick(
	servicesName string,
) *errorwrapper.Wrapper {
	_, errWrap := RunAction(
		true,
		false,
		servicestate.Reload,
		servicesName)

	return errWrap
}
