package errtype

import "gitlab.com/auk-go/core/coreinterface/errcoreinf"

func NewUsingTyper(basicErrTyper errcoreinf.BasicErrorTyper) Variation {
	errTypeVal := basicErrTyper.Value()

	return Variation(errTypeVal)
}
