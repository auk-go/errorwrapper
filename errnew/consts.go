package errnew

import "gitlab.com/auk-go/core/constants"

const (
	defaultSkipInternal = constants.One
	pathRefKeyword      = "Path"
)
