package linuxservicecmd

import (
	"gitlab.com/auk-go/enum/linuxservicestate"
	"gitlab.com/auk-go/errorwrapper"
)

func VerifyExitCode(
	serviceName string,
	expectedExitCode linuxservicestate.ExitCode,
) *errorwrapper.Wrapper {
	return GetStatusResult(serviceName).
		VerifyExitCode(expectedExitCode)
}
