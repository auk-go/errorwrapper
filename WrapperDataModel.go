package errorwrapper

import (
	"gitlab.com/auk-go/core/codestack"
	"gitlab.com/auk-go/errorwrapper/errtype"
	"gitlab.com/auk-go/errorwrapper/refs"
)

type WrapperDataModel struct {
	IsDisplayableError bool
	CurrentError       string
	ErrorType          errtype.Variation
	StackTraces        codestack.TraceCollection
	References         *refs.Collection
	HasError           bool
}

func NewDataModel(wrapper *Wrapper) WrapperDataModel {
	toModel := WrapperDataModel{}

	if wrapper == nil {
		return toModel
	}

	return transpileWrapperToModel(
		wrapper,
		&toModel)
}
