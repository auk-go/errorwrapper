package errdefer

import (
	"gitlab.com/auk-go/errorwrapper/errfunc"
	"gitlab.com/auk-go/errorwrapper/errwrappers"
)

func ErrorWrapperFuncUsingCollection(
	errorCollection *errwrappers.Collection,
	errWrapperFunc errfunc.WrapperFunc,
) (isSuccess bool) {
	errWrapper := errWrapperFunc()
	errorCollection.AddWrapperPtr(errWrapper)

	return errWrapper.IsEmpty()
}
