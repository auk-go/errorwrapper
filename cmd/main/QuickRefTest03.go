package main

import (
	"fmt"

	"gitlab.com/auk-go/errorwrapper/errtype"
	"gitlab.com/auk-go/errorwrapper/errwrappers"
)

func QuickRefTest03() {
	line := errtype.RedisKeyNotFound.ReferencesLines(
		"something wrong",
		errtype.KeyNotFound.ShortReferencesCsv("key-1"),
		errtype.DbRecordNotFound.ShortReferencesCsv("db-record-1"))

	fmt.Println(line)

	empty := errwrappers.Empty()

	fmt.Println(empty)
}
