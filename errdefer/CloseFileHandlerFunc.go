package errdefer

import (
	"os"

	"gitlab.com/auk-go/errorwrapper"
)

func CloseFileHandlerFunc(
	location string,
	existingErrorWrapper *errorwrapper.Wrapper,
	osFile *os.File,
	handlerFunc func(errorWrapper *errorwrapper.Wrapper),
) {
	finalError := CloseFile(
		location,
		existingErrorWrapper,
		osFile)

	handlerFunc(finalError)
}
