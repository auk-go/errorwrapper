package errdefer

import (
	"os"

	"gitlab.com/auk-go/errorwrapper/errnew"
	"gitlab.com/auk-go/errorwrapper/errtype"
	"gitlab.com/auk-go/errorwrapper/errwrappers"
)

func CloseFileUsingErrorCollection(
	location string,
	errorCollection *errwrappers.Collection,
	osFile *os.File,
) (isSuccess bool) {
	if osFile == nil {
		return
	}

	closerErr := osFile.Close()
	closingErrorWrapper := errnew.Path.Error(
		errtype.FileClosing,
		closerErr,
		location)

	errorCollection.AddWrapperPtr(closingErrorWrapper)

	return closingErrorWrapper.IsEmpty()
}
