package errstr

import (
	"gitlab.com/auk-go/core/codestack"
	"gitlab.com/auk-go/core/coredata/corejson"
	"gitlab.com/auk-go/core/coreinterface/serializerinf"
	"gitlab.com/auk-go/core/isany"
	"gitlab.com/auk-go/errorwrapper"
	"gitlab.com/auk-go/errorwrapper/errnew"
	"gitlab.com/auk-go/errorwrapper/errtype"
)

type newResultCreator struct{}

func (it newResultCreator) Empty() *Result {
	return &Result{}
}

func (it newResultCreator) UsingAnyItem(
	errType errtype.Variation,
	anyItem interface{},
) *Result {
	if isany.Null(anyItem) {
		return &Result{
			ErrorWrapper: errnew.Null.Simple(anyItem),
		}
	}

	jsonResult := corejson.AnyTo.SerializedJsonResult(
		anyItem)

	return it.UsingJsonResult(
		errType,
		jsonResult)
}

func (it newResultCreator) UsingSerializerFunc(
	errType errtype.Variation,
	serializerFunc func() ([]byte, error),
) *Result {
	if serializerFunc == nil {
		return &Result{
			ErrorWrapper: errnew.Null.Simple(serializerFunc),
		}
	}

	jsonResult := corejson.
		NewResult.
		UsingSerializerFunc(serializerFunc)

	return it.UsingJsonResult(
		errType,
		jsonResult)
}

func (it newResultCreator) UsingSerializer(
	errType errtype.Variation,
	serializer serializerinf.Serializer,
) *Result {
	if serializer == nil {
		return &Result{
			ErrorWrapper: errnew.Null.Simple(serializer),
		}
	}

	jsonResult := corejson.
		NewResult.
		UsingSerializer(serializer)

	return it.UsingJsonResult(
		errType,
		jsonResult)
}

func (it newResultCreator) UsingJsonerDefault(
	jsoner corejson.Jsoner,
) *Result {
	if jsoner == nil {
		return &Result{
			ErrorWrapper: errnew.Null.Simple(jsoner),
		}
	}

	return it.UsingJsonResultDefault(
		jsoner.JsonPtr())
}

func (it newResultCreator) UsingJsoner(
	errType errtype.Variation,
	jsoner corejson.Jsoner,
) *Result {
	if jsoner == nil {
		return &Result{
			ErrorWrapper: errnew.Null.Simple(jsoner),
		}
	}

	return it.UsingJsonResult(
		errType,
		jsoner.JsonPtr())
}

func (it newResultCreator) UsingJsonResult(
	errType errtype.Variation,
	jsonResult *corejson.Result,
) *Result {
	if jsonResult.HasError() {
		return &Result{
			ErrorWrapper: errnew.Type.ErrorUsingStackSkip(
				codestack.Skip1,
				errType,
				jsonResult.MeaningfulError()),
			Value: jsonResult.JsonString(),
		}
	}

	return &Result{
		Value: jsonResult.JsonString(),
	}
}

func (it newResultCreator) UsingJsonResultDefault(
	jsonResult *corejson.Result,
) *Result {
	if jsonResult.HasError() {
		return &Result{
			ErrorWrapper: errnew.Type.ErrorUsingStackSkip(
				codestack.Skip1,
				errtype.Serialize,
				jsonResult.MeaningfulError()),
			Value: jsonResult.JsonString(),
		}
	}

	return &Result{
		Value: jsonResult.JsonString(),
	}
}

func (it newResultCreator) All(
	value string,
	errType errtype.Variation,
	err error,
) *Result {
	return &Result{
		ErrorWrapper: errnew.Type.ErrorUsingStackSkip(
			codestack.Skip1,
			errType,
			err),
		Value: value,
	}
}

func (it newResultCreator) UsingBytes(
	valueAsBytes []byte,
	errType errtype.Variation,
	err error,
) *Result {
	var bytesString string
	if len(valueAsBytes) > 0 {
		bytesString = string(valueAsBytes)
	}

	return &Result{
		ErrorWrapper: errnew.Type.ErrorUsingStackSkip(
			codestack.Skip1,
			errType,
			err),
		Value: bytesString,
	}
}

func (it newResultCreator) String(
	input string,
) *Result {
	return &Result{
		Value: input,
	}
}

func (it newResultCreator) Item(
	item string,
) *Result {
	return &Result{
		Value: item,
	}
}

func (it newResultCreator) Error(
	errType errtype.Variation,
	err error,
) *Result {
	return &Result{
		ErrorWrapper: errnew.Type.ErrorUsingStackSkip(
			codestack.Skip1,
			errType,
			err),
	}
}

func (it newResultCreator) ErrorWrapper(
	errorWrapper *errorwrapper.Wrapper,
) *Result {
	return &Result{
		ErrorWrapper: errorWrapper,
	}
}

func (it newResultCreator) Create(
	result string,
	errorWrapper *errorwrapper.Wrapper,
) *Result {
	return &Result{
		Value:        result,
		ErrorWrapper: errorWrapper,
	}
}

func (it newResultCreator) ValueOnly(
	result string,
) *Result {
	return &Result{
		Value:        result,
		ErrorWrapper: nil,
	}
}
