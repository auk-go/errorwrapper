package errverifytestwrappers

import (
	"gitlab.com/auk-go/errorwrapper"
	"gitlab.com/auk-go/errorwrapper/errverify"
)

type VerifyErrorCollectionTestWrapper struct {
	InputErrorCollections []*errorwrapper.Wrapper
	Verifier              errverify.CollectionVerifier
}
