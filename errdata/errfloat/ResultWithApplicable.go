package errfloat

type ResultWithApplicable struct {
	Result
	IsApplicable bool
}
