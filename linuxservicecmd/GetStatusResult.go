package linuxservicecmd

import "gitlab.com/auk-go/enum/servicestate"

func GetStatusResult(serviceName string) *Result {
	return Run(
		servicestate.Status,
		serviceName)
}
