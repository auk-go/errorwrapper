package errint

type ResultWithApplicable struct {
	Result
	IsApplicable bool
}
