package errcmd

import (
	"gitlab.com/auk-go/core/coreinterface"
	"gitlab.com/auk-go/errorwrapper/errnew"
	"gitlab.com/auk-go/errorwrapper/errtype"
)

func getNotImplementedCmdOnceForScript(scriptDefaultStringer coreinterface.Stringer) *CmdOnce {
	notImplErr := errnew.Messages.Many(
		errtype.NotImplemented,
		scriptDefaultStringer.String())

	return &CmdOnce{
		baseCmdWrapper: BaseCmdWrapper{
			initializeErrorWrapper: notImplErr,
		},
		Cmd: nil,
	}
}
