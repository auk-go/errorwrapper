package errcmd

import (
	"strings"

	"gitlab.com/auk-go/core/constants"
	"gitlab.com/auk-go/core/coredata/stringslice"
)

func ArgsJoinPrepend(argPrepend string, args ...string) string {
	if len(args) == 0 {
		return argPrepend
	}

	slice := stringslice.PrependLineNew(argPrepend, args)

	return strings.Join(slice, constants.Space)
}
