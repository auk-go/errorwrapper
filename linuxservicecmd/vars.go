package linuxservicecmd

import "gitlab.com/auk-go/errorwrapper/errcmd"

var (
	hasService          = hasServiceCmdLookPath()
	hasSystemCtlService = hasSystemctlCmdLookPath()
	bashArgsCmdCreator  = errcmd.New.BashScript.ArgsDefault
)
