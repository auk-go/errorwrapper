package errcmd

import (
	"gitlab.com/auk-go/core/errcore"
	"gitlab.com/auk-go/enum/scripttype"
	"gitlab.com/auk-go/errorwrapper/errnew"
	"gitlab.com/auk-go/errorwrapper/errtype"
)

var (
	NewShellScript = &newCmdOnceTypedScriptsCreator{
		scriptType: scripttype.Shell,
	}
	NewBashScript = &newCmdOnceTypedScriptsCreator{
		scriptType: scripttype.Bash,
	}

	New = &newCreator{
		ShellScript: NewShellScript,
		BashScript:  NewBashScript,
	}

	cmdNilErr = errnew.Messages.Many(
		errtype.CommandExecutionNotFound,
		errcore.FailedToCreateCmdType.String(),
		"Create() *CmdOnce",
		"Failed to create inner cmd.",
	)
)
