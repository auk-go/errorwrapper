package errstr

import (
	"gitlab.com/auk-go/core/coredata/corestr"

	"gitlab.com/auk-go/errorwrapper"
)

type Hashset struct {
	*corestr.Hashset
	ErrorWrapper *errorwrapper.Wrapper
}
