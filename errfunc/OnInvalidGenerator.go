package errfunc

import (
	"gitlab.com/auk-go/core/coretaskinfo"
	"gitlab.com/auk-go/errorwrapper"
	"gitlab.com/auk-go/errorwrapper/errnew"
)

type OnInvalidGenerator struct {
	NameInfo         *coretaskinfo.Info
	IsLockRequired   bool
	OnInvalidGenFunc OnInvalidGenerateFunc
}

func (it *OnInvalidGenerator) GenerateTo(
	toPtr interface{},
) *errorwrapper.Wrapper {
	validationErrWrap := it.ValidationErrorWrap()

	if validationErrWrap.HasAnyError() {
		return validationErrWrap
	}

	if it.IsLockRequired {
		globalMutex.Lock()
		defer globalMutex.Unlock()
	}

	errWrap := it.OnInvalidGenFunc(toPtr)

	if errWrap.IsEmpty() {
		return nil
	}

	return errWrap.
		ConcatNew().
		InfoRef(it.NameInfo)
}

func (it *OnInvalidGenerator) ValidationErrorWrap() *errorwrapper.Wrapper {
	if it == nil {
		return errnew.Null.Simple(it)
	}

	if it.OnInvalidGenFunc == nil {
		return errnew.Null.WithMessage(
			"OnInvalidGenFunc - generator function is not defined",
			it.OnInvalidGenFunc)
	}

	return nil
}

func (it *OnInvalidGenerator) IsNull() bool {
	return it == nil
}

func (it *OnInvalidGenerator) IsDefined() bool {
	return it != nil && it.OnInvalidGenFunc != nil
}

func (it *OnInvalidGenerator) HasOnInvalidGenFunc() bool {
	return it != nil && it.OnInvalidGenFunc != nil
}

func (it *OnInvalidGenerator) IsOnInvalidGenFuncEmpty() bool {
	return it == nil || it.OnInvalidGenFunc == nil
}

func (it *OnInvalidGenerator) HasNameInfo() bool {
	return it != nil && it.NameInfo != nil
}

func (it *OnInvalidGenerator) NameInfoMap() map[string]string {
	if it == nil {
		return map[string]string{}
	}

	return it.NameInfo.Map()
}

func (it *OnInvalidGenerator) NameInfoLazyMap() map[string]string {
	if it == nil {
		return map[string]string{}
	}

	return it.NameInfo.LazyMap()
}

func (it *OnInvalidGenerator) NameInfoLazyMapWithPayloadsAny(
	payloadsAny interface{},
) map[string]string {
	if it == nil {
		return map[string]string{}
	}

	return it.NameInfo.LazyMapWithPayloadAsAny(payloadsAny)
}
