package errstr

import (
	"gitlab.com/auk-go/core/coredata/corestr"
	"gitlab.com/auk-go/errorwrapper"
)

type Hashmap struct {
	*corestr.Hashmap
	ErrorWrapper *errorwrapper.Wrapper
}
