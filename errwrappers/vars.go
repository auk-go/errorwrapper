package errwrappers

import (
	"sync"

	"gitlab.com/auk-go/core/coredata/coredynamic"
	"gitlab.com/auk-go/core/coredata/corejson"
	"gitlab.com/auk-go/errorwrapper/errtype"
)

var (
	defaultErrorType       = errtype.Unknown
	Deserialize            = newDeserializeCreator{} // Deserialize from payload to error collection
	ErrorInterface         = newErrorInterfaceToCollection{}
	usingBytesDeserializer = corejson.Deserialize.UsingBytes
	serializer             = corejson.Serialize.ToBytesErr
	collectionTypeName     = coredynamic.TypeName(&Collection{})
	globalMutex            = sync.Mutex{}
)
