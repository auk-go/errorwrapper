package errcmd

import (
	"bytes"
	"os/exec"
	"strings"

	"gitlab.com/auk-go/core/constants"
	"gitlab.com/auk-go/core/errcore"
	"gitlab.com/auk-go/errorwrapper"
	"gitlab.com/auk-go/errorwrapper/errnew"
	"gitlab.com/auk-go/errorwrapper/errtype"
)

type newOutputGetter struct{}

func (it newOutputGetter) CmdOutput(
	process string,
	arguments ...string,
) (allBytes []byte, err error) {
	allBytes, errWrap := it.OutputErrWrapper(
		process,
		arguments...)

	return allBytes, errWrap.CompiledErrorWithStackTraces()
}

func (it newOutputGetter) OutputErrWrapper(
	process string,
	arguments ...string,
) (allBytes []byte, errWrap *errorwrapper.Wrapper) {
	cmd := exec.Command(
		process, arguments...)

	wholeCommand := ProcessArgsJoinAppend(
		process, arguments...)

	if cmd == nil {
		return nil, errnew.Message.Default(
			errtype.NotFoundProcess,
			wholeCommand)
	}

	buffer := &bytes.Buffer{}
	cmd.Stderr = buffer
	rawErrCollection := errcore.RawErrCollection{}

	allBytes, err := cmd.Output()

	rawErrCollection.AddError(err)
	if buffer.Len() > 0 {
		rawErrCollection.AddString(buffer.String())
	}

	if rawErrCollection.HasError() {
		return allBytes, errnew.Error.TypeMsg(
			errtype.FailedProcess,
			rawErrCollection.CompiledError(),
			wholeCommand)
	}

	return allBytes, nil
}

func (it newOutputGetter) OutputStringErrWrapper(
	process string,
	arguments ...string,
) (output string, errWrap *errorwrapper.Wrapper) {
	allBytes, errWrap := it.OutputErrWrapper(
		process,
		arguments...)

	if len(allBytes) == 0 {
		return "", errWrap
	}

	return string(allBytes), errWrap
}

func (it newOutputGetter) OutputStringsErrWrapper(
	process string,
	arguments ...string,
) (outputLines []string, errWrap *errorwrapper.Wrapper) {
	output, errWrap := it.OutputStringErrWrapper(
		process,
		arguments...)

	if output == "" {
		return []string{}, errWrap
	}

	return strings.Split(output, constants.DefaultLine), errWrap
}
