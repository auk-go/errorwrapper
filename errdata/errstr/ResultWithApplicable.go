package errstr

type ResultWithApplicable struct {
	Result
	IsApplicable bool
}
