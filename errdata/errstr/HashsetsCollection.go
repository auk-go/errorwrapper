package errstr

import (
	"gitlab.com/auk-go/core/coredata/corestr"

	"gitlab.com/auk-go/errorwrapper"
)

type HashsetsCollection struct {
	*corestr.HashsetsCollection
	ErrorWrapper *errorwrapper.Wrapper
}
