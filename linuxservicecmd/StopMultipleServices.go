package linuxservicecmd

import (
	"gitlab.com/auk-go/enum/servicestate"
	"gitlab.com/auk-go/errorwrapper/errwrappers"
)

func StopMultipleServices(
	isDetailedError,
	isContinueOnErr bool,
	errCollection *errwrappers.Collection,
	servicesNames ...string,
) (isSuccess bool) {
	return RunServices(
		isDetailedError,
		isContinueOnErr,
		false,
		errCollection,
		servicestate.Stop,
		servicesNames...)
}
