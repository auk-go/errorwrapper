package linuxservicecmd

import (
	"gitlab.com/auk-go/enum/linuxservicestate"
	"gitlab.com/auk-go/enum/servicestate"
)

func GetStatus(serviceName string) linuxservicestate.ExitCode {
	res := Run(
		servicestate.Status,
		serviceName)

	return res.ExitCode
}
