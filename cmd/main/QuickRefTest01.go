package main

import (
	"fmt"

	"gitlab.com/auk-go/errorwrapper/errtype"
	"gitlab.com/auk-go/errorwrapper/refs"
)

func QuickRefTest01() {
	line := refs.QuickCompileStringDefaultEachLine(
		refs.NewQuickReference(errtype.DbUpdateFailed, "RecordKey", "key-5"),
		refs.NewQuickReference(errtype.KeyNotFound, "Key-1"),
		refs.NewQuickReference(errtype.KeyMissing, "Key-2"))

	fmt.Println(line)
}
