package errfunc

import (
	"gitlab.com/auk-go/enum/linuxtype"
)

type LinuxWrapperAction struct {
	LinuxType linuxtype.Variant
	WrapperFunc
}
