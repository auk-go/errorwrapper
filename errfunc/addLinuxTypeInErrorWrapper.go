package errfunc

import (
	"gitlab.com/auk-go/errorwrapper"
	"gitlab.com/auk-go/errorwrapper/errwrappers"
	"gitlab.com/auk-go/errorwrapper/ref"
)

func convertWrapperWithAdditionalKeyMessageFunc(
	wrapperFunc WrapperFunc,
	errCollection *errwrappers.Collection,
	key string,
) func() *errorwrapper.Wrapper {
	return func() *errorwrapper.Wrapper {
		errWrapper := wrapperFunc()

		if errWrapper.HasError() {
			errCollection.AddWrapperWithAdditionalRefs(
				errWrapper,
				ref.Value{
					Variable: "LinuxType",
					Value:    key,
				})
		}

		return nil
	}
}
