package errcmd

import (
	"strings"

	"gitlab.com/auk-go/core/constants"
	"gitlab.com/auk-go/core/coredata/stringslice"
)

func ProcessArgsJoinAppend(process string, args ...string) string {
	if len(args) == 0 {
		return process
	}

	slice := stringslice.AppendLineNew(args, process)

	return strings.Join(slice, constants.Space)
}
