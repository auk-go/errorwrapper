package creationtests

import (
	"gitlab.com/auk-go/core/coreinterface/errcoreinf"
	"gitlab.com/auk-go/core/coretests"
)

type TestCaseWrapper struct {
	coretests.BaseTestCase
}

func (it TestCaseWrapper) ExpectedAsStrings() []string {
	return it.Expected().([]string)
}

func (it TestCaseWrapper) ArrangeAsBaseErrorOrCollectionWrapper() []errcoreinf.BaseErrorOrCollectionWrapper {
	return it.ArrangeInput.([]errcoreinf.BaseErrorOrCollectionWrapper)
}
