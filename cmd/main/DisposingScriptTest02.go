package main

import (
	"fmt"

	"gitlab.com/auk-go/enum/scripttype"
	"gitlab.com/auk-go/errorwrapper/errcmd"
)

func DisposingScriptTest02() {
	cmdOnceCollection := errcmd.NewCmdOnceCollectionUsingLinesOfScripts(
		scripttype.Cmd,
		"dir /w")

	fmt.Println(cmdOnceCollection.Strings())
	cmdOnceCollection.Dispose()
}
