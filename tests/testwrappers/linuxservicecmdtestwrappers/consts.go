package linuxservicecmdtestwrappers

const (
	ServiceExistCron     = "cron"
	ServiceExistIptables = "iptables"
	ServiceNonExistUfwx  = "ufwx"
	ServiceNonExistUfwx2 = "ufwx2"
)
