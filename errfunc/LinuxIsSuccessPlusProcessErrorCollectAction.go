package errfunc

import (
	"gitlab.com/auk-go/enum/linuxtype"
)

type LinuxIsSuccessPlusProcessErrorCollectAction struct {
	LinuxType linuxtype.Variant
	IsSuccessProcessorCollectorFunc
}
