package errdefer

import (
	"gitlab.com/auk-go/errorwrapper/errtype"
	"gitlab.com/auk-go/errorwrapper/errwrappers"
)

func ErrorWithMessagesUsingCollection(
	errorCollection *errwrappers.Collection,
	errType errtype.Variation,
	err error,
	messages ...string,
) (isSuccess bool) {
	errorCollection.AddErrorWithMessages(
		errType,
		err,
		messages...)

	return err == nil
}
