package linuxservicecmd

import (
	"gitlab.com/auk-go/enum/servicestate"
	"gitlab.com/auk-go/errorwrapper"
)

func StartQuick(
	servicesName string,
) *errorwrapper.Wrapper {
	_, errWrap := RunAction(
		true,
		false,
		servicestate.Start,
		servicesName)

	return errWrap
}
