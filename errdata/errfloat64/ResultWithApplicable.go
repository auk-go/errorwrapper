package errfloat64

type ResultWithApplicable struct {
	Result
	IsApplicable bool
}
