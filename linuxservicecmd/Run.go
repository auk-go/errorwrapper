package linuxservicecmd

import (
	"gitlab.com/auk-go/core/codestack"
	"gitlab.com/auk-go/enum/linuxservicestate"
	"gitlab.com/auk-go/enum/servicestate"
)

func Run(
	action servicestate.Action,
	serviceName string,
) *Result {
	cmd := getCmdOnce(action, serviceName)
	cmdResult := cmd.CompiledResult()
	compiledCode := linuxservicestate.NewCode(cmdResult.ExitCode)
	errWrapper := cmdResult.ErrorWrapper()

	if errWrapper.HasError() {
		errWrapper = errWrapper.
			ConcatNew().
			MsgRefTwo(
				codestack.Skip1,
				"Service execution failed",
				"Service",
				serviceName,
				"Action",
				action.NameValue())
	}

	return &Result{
		Request: Request{
			ServiceName: serviceName,
			Action:      action,
		},
		CmdOnce:      cmd,
		ExitCode:     compiledCode,
		ErrorWrapper: errWrapper,
	}
}
