package errfunc

import (
	"gitlab.com/auk-go/enum/linuxtype"
	"gitlab.com/auk-go/errorwrapper"
	"gitlab.com/auk-go/errorwrapper/errwrappers"
	"gitlab.com/auk-go/errorwrapper/ref"
)

func convertWrapperWithAdditionalLinuxTypeMessageFunc(
	variation linuxtype.Variant,
	errCollection *errwrappers.Collection,
	wrapperFunc WrapperFunc,
) func() *errorwrapper.Wrapper {
	return func() *errorwrapper.Wrapper {
		errWrapper := wrapperFunc()

		if errWrapper.HasError() {
			errCollection.AddWrapperWithAdditionalRefs(
				errWrapper,
				ref.Value{
					Variable: "LinuxType",
					Value:    variation.Name(),
				})
		}

		return nil
	}
}
