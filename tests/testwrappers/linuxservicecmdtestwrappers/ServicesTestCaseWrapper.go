package linuxservicecmdtestwrappers

import "gitlab.com/auk-go/errorwrapper/linuxservicecmd"

type ServicesTestCaseWrapper struct {
	Header string
	linuxservicecmd.ServicesInstruction
	ErrorValidation []string
}
