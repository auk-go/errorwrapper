package errcmd

import (
	"gitlab.com/auk-go/errorwrapper/internal/consts"
)

const (
	InvalidExitCode             = consts.InvalidExitCode
	SuccessfullyRunningExitCode = consts.CmdSuccessfullyRunningExitCode
	SingleLineScriptsJoiner     = " && " // " && "
	changeDirSpace              = "cd "
	ScriptsMultiLineJoiner      = " && \\ \n" // " && \\ \n"
)
