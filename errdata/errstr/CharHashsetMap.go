package errstr

import (
	"gitlab.com/auk-go/core/coredata/corestr"

	"gitlab.com/auk-go/errorwrapper"
)

type CharHashsetMap struct {
	*corestr.CharHashsetMap
	ErrorWrapper *errorwrapper.Wrapper
}
