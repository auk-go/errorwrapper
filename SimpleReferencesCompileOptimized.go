package errorwrapper

import (
	"fmt"

	"gitlab.com/auk-go/core/constants"
	"gitlab.com/auk-go/core/corecsv"
	"gitlab.com/auk-go/errorwrapper/errconsts"
	"gitlab.com/auk-go/errorwrapper/errtype"
)

// SimpleReferencesCompileOptimized
//
// errconsts.SimpleReferenceCompileOptimizedFormat = `%typeName (..., "reference")`
func SimpleReferencesCompileOptimized(
	errType errtype.Variation,
	references ...interface{},
) string {
	variantStruct := errType.VariantStructurePtr()
	compiledString := corecsv.AnyItemsToCsvString(
		constants.CommaSpace,
		true,
		false,
		references...)

	if compiledString == constants.EmptyString {
		return variantStruct.Name
	}

	return fmt.Sprintf(
		errconsts.SimpleReferenceCompileOptimizedFormat,
		variantStruct.Name,
		compiledString)
}
