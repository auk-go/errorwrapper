package errcmd

import (
	"strings"

	"gitlab.com/auk-go/core/constants"
	"gitlab.com/auk-go/core/coredata/stringslice"
)

func ArgsJoinWithSingle(arg1 string, args ...string) string {
	if len(args) == 0 {
		return constants.EmptyString
	}

	newSlice := stringslice.AppendLineNew(
		args,
		arg1)

	return strings.Join(newSlice, constants.Space)
}
