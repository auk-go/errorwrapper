package errbyte

import (
	"gitlab.com/auk-go/core/codestack"
	"gitlab.com/auk-go/errorwrapper"
	"gitlab.com/auk-go/errorwrapper/errnew"
	"gitlab.com/auk-go/errorwrapper/errtype"
)

type newResultTwoCreator struct{}

func (it newResultTwoCreator) Empty() *Result2 {
	return &Result2{}
}

func (it newResultTwoCreator) Item(
	input byte,
) *Result2 {
	return &Result2{
		Result: Result{
			Value: input,
		},
	}
}

func (it newResultTwoCreator) Error(
	errType errtype.Variation,
	err error,
) *Result2 {
	return &Result2{
		Result: Result{
			ErrorWrapper: errnew.Type.ErrorUsingStackSkip(
				codestack.Skip1,
				errType,
				err),
		},
	}
}

func (it newResultTwoCreator) ValueOnly(
	result1, result2 byte,
) *Result2 {
	return &Result2{
		Result: Result{
			Value: result1,
		},
		Value2: result2,
	}
}

func (it newResultTwoCreator) Create(
	result,
	result2 byte,
	wrapper *errorwrapper.Wrapper,
) *Result2 {
	return &Result2{
		Result: Result{
			Value:        result,
			ErrorWrapper: wrapper,
		},
		Value2: result2,
	}
}

func (it newResultTwoCreator) ErrorWrapper(
	errorWrapper *errorwrapper.Wrapper,
) *Result2 {
	return &Result2{
		Result: Result{
			ErrorWrapper: errorWrapper,
		},
	}
}
