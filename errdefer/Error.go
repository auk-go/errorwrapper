package errdefer

import (
	"gitlab.com/auk-go/errorwrapper"
	"gitlab.com/auk-go/errorwrapper/errnew"
	"gitlab.com/auk-go/errorwrapper/errtype"
)

func Error(
	existingErrorWrapper *errorwrapper.Wrapper, // could be nil
	errType errtype.Variation,
	err error,
) *errorwrapper.Wrapper {
	closingErrorWrapper := errnew.Type.Error(
		errType,
		err)

	return mergeErrorWrapper(
		existingErrorWrapper,
		closingErrorWrapper)
}
