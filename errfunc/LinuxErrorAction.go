package errfunc

import (
	"gitlab.com/auk-go/enum/linuxtype"
)

type LinuxErrorAction struct {
	LinuxType linuxtype.Variant
	SimpleErrorFunc
}
