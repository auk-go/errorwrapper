package errstr

import (
	"gitlab.com/auk-go/core/coredata/corestr"

	"gitlab.com/auk-go/errorwrapper"
)

type CharCollectionMap struct {
	*corestr.CharCollectionMap
	ErrorWrapper *errorwrapper.Wrapper
}
