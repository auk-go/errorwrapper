package linuxservicecmd

import (
	"gitlab.com/auk-go/enum/servicestate"
	"gitlab.com/auk-go/errorwrapper"
)

func Restart(
	isDetailedError,
	isIgnoreUnknownService bool,
	action servicestate.Action,
	servicesName string,
) (*Result, *errorwrapper.Wrapper) {
	return RunAction(
		isDetailedError,
		isIgnoreUnknownService,
		action,
		servicesName)
}
