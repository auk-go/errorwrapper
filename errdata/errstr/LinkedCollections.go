package errstr

import (
	"gitlab.com/auk-go/core/coredata/corestr"

	"gitlab.com/auk-go/errorwrapper"
)

// LinkedCollections TODO constructors
type LinkedCollections struct {
	*corestr.LinkedCollections
	ErrorWrapper *errorwrapper.Wrapper
}
