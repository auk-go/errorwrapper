package errdefer

import (
	"gitlab.com/auk-go/errorwrapper"
	"gitlab.com/auk-go/errorwrapper/errfunc"
)

func ErrorWrapperFunc(
	existingErrorWrapper *errorwrapper.Wrapper,
	errWrapperFunc errfunc.WrapperFunc,
) *errorwrapper.Wrapper {
	errWrapper := errWrapperFunc()

	return mergeErrorWrapper(
		existingErrorWrapper,
		errWrapper)
}
