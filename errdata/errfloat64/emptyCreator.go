package errfloat64

import "gitlab.com/auk-go/errorwrapper"

type emptyCreator struct{}

func (it *emptyCreator) Result() *Result {
	return &Result{}
}

func (it *emptyCreator) Results() *Results {
	return &Results{}
}

func (it *emptyCreator) Result2() *Result2 {
	return &Result2{}
}

func (it *emptyCreator) Result3() *Result3 {
	return &Result3{}
}

func (it *emptyCreator) ResultsWithErrorCollection() *ResultsWithErrorCollection {
	return &ResultsWithErrorCollection{}
}

func (it *emptyCreator) ResultWithApplicable() *ResultWithApplicable {
	return &ResultWithApplicable{}
}

func (it *emptyCreator) ResultWithApplicable2() *ResultWithApplicable2 {
	return &ResultWithApplicable2{}
}

func (it *emptyCreator) ResultWithError(errorWrapper *errorwrapper.Wrapper) *Result {
	return &Result{
		ErrorWrapper: errorWrapper,
	}
}

func (it *emptyCreator) ResultsWithError(errorWrapper *errorwrapper.Wrapper) *Results {
	return &Results{
		ErrorWrapper: errorWrapper,
	}
}

func (it *emptyCreator) ResultWithValue(value float64) *Result {
	return &Result{
		Value: value,
	}
}

func (it *emptyCreator) ResultsWithValue(values []float64) *Results {
	return &Results{
		Values: values,
	}
}
