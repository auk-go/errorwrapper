package errdefer

import (
	"gitlab.com/auk-go/errorwrapper/errtype"
	"gitlab.com/auk-go/errorwrapper/errwrappers"
)

func ErrorUsingCollection(
	errorCollection *errwrappers.Collection,
	errType errtype.Variation,
	err error,
) {
	errorCollection.AddTypeError(
		errType,
		err)
}
