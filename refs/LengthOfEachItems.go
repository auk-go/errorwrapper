package refs

import (
	"gitlab.com/auk-go/core/constants"
	"gitlab.com/auk-go/errorwrapper/ref"
)

func LengthOfEachItems(manyCollections [][]ref.Value) int {
	if len(manyCollections) == 0 {
		return constants.Zero
	}

	length := constants.Zero

	for _, collection := range manyCollections {
		length += len(collection)
	}

	return length
}
