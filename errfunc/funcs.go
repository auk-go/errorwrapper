package errfunc

import (
	"gitlab.com/auk-go/core/coredata/coredynamic"
	"gitlab.com/auk-go/core/coredata/corepayload"
	"gitlab.com/auk-go/errorwrapper"
	"gitlab.com/auk-go/errorwrapper/errdata/errbool"
	"gitlab.com/auk-go/errorwrapper/errdata/errstr"
	"gitlab.com/auk-go/errorwrapper/errtype"
	"gitlab.com/auk-go/errorwrapper/errwrappers"
)

type (
	SimpleErrorFunc                 func() error
	WrapperFunc                     func() *errorwrapper.Wrapper
	BoolReturnFunc                  func() *errbool.Result
	CollectionReturnFunc            func() *errstr.Collection
	CollectorFunc                   func(errorCollection *errwrappers.Collection)
	IsSuccessCollectorFunc          func(errorCollection *errwrappers.Collection) (isSuccess bool)
	OnInvalidGenerateFunc           func(toPtr interface{}) *errorwrapper.Wrapper
	AnyProcessorFunc                func(from interface{}) (toProcessed interface{}, wrapper *errorwrapper.Wrapper)
	PayloadValidatorFunc            func(payload *corepayload.PayloadWrapper) *errorwrapper.Wrapper
	AnyItemValidatorFunc            func(anyInput interface{}) *errorwrapper.Wrapper
	AnyItemProcessorFunc            func(anyInput interface{}) *errorwrapper.Wrapper
	PayloadWrapperExecutorFunc      func(payloadWrapper *corepayload.PayloadWrapper) *errorwrapper.Wrapper
	BytesExecutorFunc               func(payloads []byte) *errorwrapper.Wrapper
	IsSuccessProcessorCollectorFunc func(
		dynamicIn coredynamic.Dynamic,
		errorCollection *errwrappers.Collection,
	) (isSuccess bool)
	ConvertErrorFuncToWrapperFunc func(
		errorType errtype.Variation,
		simpleErrFunc SimpleErrorFunc,
	) WrapperFunc
)
