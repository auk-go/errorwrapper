package errcmd

import (
	"gitlab.com/auk-go/core/codestack"
	"gitlab.com/auk-go/enum/osmixtype"
	"gitlab.com/auk-go/errorwrapper"
	"gitlab.com/auk-go/errorwrapper/errtype"
)

func CurrentOsDetails() (*osmixtype.OperatingSystemDetail, *errorwrapper.Wrapper) {
	osDetails, err := osmixtype.GetCurrentOsDetail()

	if err != nil {
		return nil, errorwrapper.NewMsgDisplayErrorNoReference(
			codestack.Skip1,
			errtype.OperatingSystemRelated,
			err.Error())
	}

	return osDetails, nil
}
