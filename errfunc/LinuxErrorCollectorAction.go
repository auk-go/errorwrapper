package errfunc

import (
	"gitlab.com/auk-go/enum/linuxtype"
)

type LinuxErrorCollectorAction struct {
	LinuxType linuxtype.Variant
	CollectorFunc
}
