package errany

type ResultWithApplicable struct {
	Result
	IsApplicable bool
}
